"""
WSGI config for myproject project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/2.2/howto/deployment/wsgi/
"""

import os
import sys
from django.core.wsgi import get_wsgi_application


#/usr/local/var/www/my_first_app/myproject/myproject
sys.path.append(os.path.dirname(os.path.abspath(__file__)))
sys.path.append(os.path.dirname(os.path.abspath(__file__)) + '/..')
sys.path.append(os.path.dirname(os.path.abspath(__file__)) + '/../../lib/python3.7/site-packages')

# sys.path.append('/usr/local/var/www/my_first_app/myproject')
# sys.path.append('/usr/local/var/www/my_first_app/myproject/myproject')
# sys.path.append('/usr/local/var/www/my_first_app/lib/python3.7/site-packages')

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'myproject.settings')

application = get_wsgi_application()
