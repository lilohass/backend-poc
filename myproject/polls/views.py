from django.shortcuts import render

from django.http import HttpResponse


def index(request):
    my_string: str = "My String Value"
    return HttpResponse(my_string)
